msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:33+0000\n"
"PO-Revision-Date: 2024-02-29 12:56\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/calligra/calligrawords.pot\n"
"X-Crowdin-File-ID: 5475\n"

#. i18n: ectx: Menu (file)
#: part/calligrawords.rc:4 part/calligrawords_readonly.rc:4
#, kde-format
msgid "&File"
msgstr "文件(&F)"

#. i18n: ectx: Menu (edit)
#: part/calligrawords.rc:11 part/calligrawords_readonly.rc:7
#, kde-format
msgid "&Edit"
msgstr "编辑(&E)"

#. i18n: ectx: Menu (view)
#: part/calligrawords.rc:30 part/calligrawords_readonly.rc:13
#, kde-format
msgid "&View"
msgstr "视图(&V)"

#. i18n: ectx: Menu (view_displaymodes)
#. i18n: ectx: Menu (view)
#: part/calligrawords.rc:32 part/calligrawords_readonly.rc:15
#, kde-format
msgid "&Display Mode"
msgstr "显示模式(&D)"

#. i18n: ectx: Menu (semantic)
#: part/calligrawords.rc:62
#, kde-format
msgid "Semantics"
msgstr "语义学"

#. i18n: ectx: Menu (format)
#: part/calligrawords.rc:74
#, kde-format
msgid "S&tyles"
msgstr "样式(&T)"

#. i18n: ectx: Menu (settings)
#: part/calligrawords.rc:78
#, kde-format
msgid "&Settings"
msgstr "设置(&S)"

#. i18n: ectx: Menu (SpellCheck)
#: part/calligrawords.rc:79
#, kde-format
msgid "Spellcheck"
msgstr "拼写检查"

#. i18n: ectx: Menu (AutoCorrection)
#: part/calligrawords.rc:83
#, kde-format
msgid "AutoCorrection"
msgstr ""

#. i18n: ectx: ToolBar (edit_toolbar)
#: part/calligrawords.rc:94
#, kde-format
msgid "Edit"
msgstr "编辑"

#. i18n: ectx: Menu (check_spell_list)
#: part/calligrawords.rc:150
#, kde-format
msgid "Spell Check Result"
msgstr "拼写检查结果"

#. i18n: ectx: Menu (variable_list)
#: part/calligrawords.rc:177
#, kde-format
msgid "Change Variable To"
msgstr "将变量更改为"

#. i18n: ectx: Menu (tools)
#: part/calligrawords_readonly.rc:30
#, kde-format
msgid "&Tools"
msgstr "工具(&T)"

#: part/commands/KWChangePageStyleCommand.cpp:11
msgctxt "(qtundo-format)"
msgid "Set Page Style"
msgstr "设定页面样式"

#: part/commands/KWNewPageStyleCommand.cpp:12
msgctxt "(qtundo-format)"
msgid "Insert Page Style"
msgstr "插入页面样式"

#: part/commands/KWPageStylePropertiesCommand.cpp:16
msgctxt "(qtundo-format)"
msgid "Page Properties"
msgstr "页面属性"

#: part/commands/KWShapeCreateCommand.cpp:42
msgctxt "(qtundo-format)"
msgid "Create shape"
msgstr "创建形状"

#: part/dialogs/KWAnchoringProperties.cpp:422
#, kde-format
msgid "Baseline"
msgstr "基准线"

#: part/dialogs/KWAnchoringProperties.cpp:423
#: part/dialogs/KWAnchoringProperties.cpp:434
#, kde-format
msgid "Character"
msgstr "字符"

#: part/dialogs/KWAnchoringProperties.cpp:426
#, kde-format
msgid "Row"
msgstr "行"

#: part/dialogs/KWAnchoringProperties.cpp:427
#: part/dialogs/KWAnchoringProperties.cpp:435
#, kde-format
msgid "Page (entire) area"
msgstr "页面整体区域"

#: part/dialogs/KWAnchoringProperties.cpp:428
#: part/dialogs/KWAnchoringProperties.cpp:436
#, kde-format
msgid "Page text area"
msgstr "页面文本区域"

#: part/dialogs/KWAnchoringProperties.cpp:429
#: part/dialogs/KWAnchoringProperties.cpp:443
#, kde-format
msgid "Paragraph area"
msgstr "段落区域"

#: part/dialogs/KWAnchoringProperties.cpp:430
#: part/dialogs/KWAnchoringProperties.cpp:444
#, kde-format
msgid "Paragraph text area"
msgstr "段落文本区域"

#: part/dialogs/KWAnchoringProperties.cpp:437
#, kde-format
msgid "Left page border"
msgstr "左侧页面边框"

#: part/dialogs/KWAnchoringProperties.cpp:438
#, kde-format
msgid "Right page border"
msgstr "右侧页面边框"

#: part/dialogs/KWAnchoringProperties.cpp:445
#, kde-format
msgid "Right paragraph border"
msgstr "右侧段落边框"

#: part/dialogs/KWAnchoringProperties.cpp:446
#, kde-format
msgid "Left paragraph border"
msgstr "左侧段落边框"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#: part/dialogs/KWAnchoringProperties.ui:17
#, kde-format
msgid "Shape relation to text is:"
msgstr "形状与文本关系："

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorAsCharacter)
#: part/dialogs/KWAnchoringProperties.ui:23
#, kde-format
msgid "Inline with text (behaves as a character)"
msgstr "内嵌于文本(如同字符)"

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorToCharacter)
#: part/dialogs/KWAnchoringProperties.ui:30
#, kde-format
msgid "Floating, but anchored to a character position"
msgstr "浮动，但固定于字符位置"

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorParagraph)
#: part/dialogs/KWAnchoringProperties.ui:37
#, kde-format
msgid "Floating, but anchored to a paragraph"
msgstr "浮动，但固定于段落位置"

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorPage)
#: part/dialogs/KWAnchoringProperties.ui:44
#, kde-format
msgid "Floating free (not anchored to text)"
msgstr "自由浮动(不固定于文本)"

#. i18n: ectx: property (title), widget (QGroupBox, grpVert)
#: part/dialogs/KWAnchoringProperties.ui:54
#, kde-format
msgid "Vertical Positioning"
msgstr "垂直定位"

#. i18n: ectx: property (text), widget (QRadioButton, rTop)
#: part/dialogs/KWAnchoringProperties.ui:60
#, kde-format
msgid "Top align with:"
msgstr "上对齐："

#. i18n: ectx: property (text), widget (QRadioButton, rVCenter)
#. i18n: ectx: property (text), widget (QRadioButton, rHCenter)
#: part/dialogs/KWAnchoringProperties.ui:77
#: part/dialogs/KWAnchoringProperties.ui:186
#, kde-format
msgid "Center align with:"
msgstr "中心对齐："

#. i18n: ectx: property (text), widget (QRadioButton, rBottom)
#: part/dialogs/KWAnchoringProperties.ui:87
#, kde-format
msgid "Bottom align with:"
msgstr "下对齐："

#. i18n: ectx: property (text), widget (QRadioButton, rVOffset)
#. i18n: ectx: property (text), widget (QRadioButton, rHOffset)
#: part/dialogs/KWAnchoringProperties.ui:97
#: part/dialogs/KWAnchoringProperties.ui:206
#, kde-format
msgid "Offset"
msgstr "偏移"

#. i18n: ectx: property (text), widget (QLabel, lVOffset)
#. i18n: ectx: property (text), widget (QLabel, lHOffset)
#: part/dialogs/KWAnchoringProperties.ui:119
#: part/dialogs/KWAnchoringProperties.ui:228
#, kde-format
msgid "from:"
msgstr "从："

#. i18n: ectx: property (title), widget (QGroupBox, grpHoriz)
#: part/dialogs/KWAnchoringProperties.ui:163
#, kde-format
msgid "Horizontal Positioning"
msgstr "水平对齐"

#. i18n: ectx: property (text), widget (QRadioButton, rLeft)
#: part/dialogs/KWAnchoringProperties.ui:169
#, kde-format
msgid "Left align with:"
msgstr "左对齐："

#. i18n: ectx: property (text), widget (QRadioButton, rRight)
#: part/dialogs/KWAnchoringProperties.ui:196
#, kde-format
msgid "Right align with:"
msgstr "右对齐："

#: part/dialogs/KWConfigureDialog.cpp:28
#, kde-format
msgid "Configure"
msgstr "配置"

#: part/dialogs/KWConfigureDialog.cpp:32 part/dialogs/KWConfigureDialog.cpp:33
#, kde-format
msgid "Misc"
msgstr "杂项"

#: part/dialogs/KWConfigureDialog.cpp:37 part/dialogs/KWConfigureDialog.cpp:38
#, kde-format
msgid "Grid"
msgstr "网格"

#: part/dialogs/KWConfigureDialog.cpp:44
#, kde-format
msgctxt "@title:tab Document settings page"
msgid "Document"
msgstr "文档"

#: part/dialogs/KWConfigureDialog.cpp:45
#, kde-format
msgid "Document Settings"
msgstr "文档设置"

#: part/dialogs/KWConfigureDialog.cpp:49
#, kde-format
msgctxt "@title:tab Author page"
msgid "Author"
msgstr "作者"

#: part/dialogs/KWConfigureDialog.cpp:50
#, kde-format
msgid "Author"
msgstr "作者"

#. i18n: ectx: property (windowTitle), widget (QWidget, CreateBookmark)
#: part/dialogs/KWCreateBookmark.ui:13
#: part/dialogs/KWCreateBookmarkDialog.cpp:37
#, kde-format
msgid "Create New Bookmark"
msgstr "创建新书签"

#. i18n: ectx: property (text), widget (QLabel, lExplain)
#: part/dialogs/KWCreateBookmark.ui:20
#, kde-format
msgid ""
"Bookmarks allow you to jump between parts of your document.\n"
"Please provide the name of your bookmark."
msgstr ""
"书签允许您在您文档的各个部分间跳转。\n"
"请提供您书签的名称。"

#. i18n: ectx: property (text), widget (QLabel, lName)
#: part/dialogs/KWCreateBookmark.ui:38
#, kde-format
msgid "Name:"
msgstr "名称："

#. i18n: ectx: property (text), widget (QLabel, columnsLabel)
#: part/dialogs/KWDocumentColumns.ui:24
#, kde-format
msgid "Columns:"
msgstr "栏数："

#. i18n: ectx: property (text), widget (QLabel, spacingLabel)
#: part/dialogs/KWDocumentColumns.ui:44
#, kde-format
msgid "Column spacing:"
msgstr "栏间距："

#: part/dialogs/KWFrameConnectSelector.cpp:101
#, kde-format
msgid "frameset"
msgstr "框架集"

#: part/dialogs/KWFrameConnectSelector.cpp:126
#, kde-format
msgid "No framesets in document"
msgstr "文档中没有框架集"

#. i18n: ectx: property (text), widget (QLabel, label)
#: part/dialogs/KWFrameConnectSelector.ui:67
#, kde-format
msgid "Name of frameset:"
msgstr "框架集名称："

#. i18n: ectx: property (text), widget (QRadioButton, newRadio)
#: part/dialogs/KWFrameConnectSelector.ui:82
#, kde-format
msgid "Create a new frameset:"
msgstr "创建新框架集："

#. i18n: ectx: property (text), widget (QRadioButton, existingRadio)
#: part/dialogs/KWFrameConnectSelector.ui:89
#, kde-format
msgid "Select existing frameset to connect to:"
msgstr "连接到框架集："

#. i18n: ectx: property (text), widget (QTreeWidget, framesList)
#: part/dialogs/KWFrameConnectSelector.ui:122
#, kde-format
msgid "Frameset Name"
msgstr "框架集名称"

#: part/dialogs/KWFrameDialog.cpp:29
#, kde-format
msgid "Smart Positioning"
msgstr "智能定位"

#: part/dialogs/KWFrameDialog.cpp:33 part/dialogs/KWShapeConfigFactory.cpp:62
#, kde-format
msgid "Text Run Around"
msgstr "文本环绕"

#: part/dialogs/KWFrameDialog.cpp:41 part/dialogs/KWShapeConfigFactory.cpp:26
#, kde-format
msgid "Connect Text Frames"
msgstr "连接文本框架"

#: part/dialogs/KWFrameDialog.cpp:66
msgctxt "(qtundo-format)"
msgid "Change Shape Properties"
msgstr "更改形状属性"

#. i18n: ectx: property (title), widget (QGroupBox, grpAnchor)
#: part/dialogs/KWInsertImage.ui:17
#, kde-format
msgid "Anchor to..."
msgstr "固定于..."

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorPage)
#: part/dialogs/KWInsertImage.ui:23
#, kde-format
msgid "&Page"
msgstr "页面(&P)"

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorParagraph)
#: part/dialogs/KWInsertImage.ui:30
#, kde-format
msgid "Para&graph"
msgstr "段落(&G)"

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorToCharacter)
#: part/dialogs/KWInsertImage.ui:40
#, kde-format
msgid "&To Character"
msgstr "至字符(&T)"

#. i18n: ectx: property (text), widget (QRadioButton, rAnchorAsCharacter)
#: part/dialogs/KWInsertImage.ui:47
#, kde-format
msgid "&As Character"
msgstr "作为字符(&A)"

#. i18n: ectx: property (title), widget (QGroupBox, grpHAlign)
#: part/dialogs/KWInsertImage.ui:59
#, kde-format
msgid "Horizontal Alignment"
msgstr "水平对齐"

#. i18n: ectx: property (text), widget (QRadioButton, rAlignLeft)
#: part/dialogs/KWInsertImage.ui:65
#, kde-format
msgid "&Left"
msgstr "左(&L)"

#. i18n: ectx: property (text), widget (QRadioButton, rAlignCenter)
#: part/dialogs/KWInsertImage.ui:75
#, kde-format
msgid "&Center"
msgstr "居中(&C)"

#. i18n: ectx: property (text), widget (QRadioButton, rAlignRight)
#: part/dialogs/KWInsertImage.ui:82
#, kde-format
msgid "&Right"
msgstr "右(&R)"

#. i18n: ectx: property (title), widget (QGroupBox, grpVAlign)
#: part/dialogs/KWInsertImage.ui:92
#, kde-format
msgid "Vertical Alignment"
msgstr "垂直对齐"

#. i18n: ectx: property (text), widget (QRadioButton, rAlignTop)
#: part/dialogs/KWInsertImage.ui:98
#, kde-format
msgid "&Top"
msgstr "上(&T)"

#. i18n: ectx: property (text), widget (QRadioButton, rAlignMiddle)
#: part/dialogs/KWInsertImage.ui:105
#, kde-format
msgid "&Middle"
msgstr "居中(&M)"

#. i18n: ectx: property (text), widget (QRadioButton, rAlignBottom)
#: part/dialogs/KWInsertImage.ui:115
#, kde-format
msgid "&Bottom"
msgstr "下(&B)"

#. i18n: ectx: property (title), widget (QGroupBox, grpWrap)
#: part/dialogs/KWInsertImage.ui:127
#, kde-format
msgid "Text Wrap-around"
msgstr "文本环绕"

#. i18n: ectx: property (text), widget (QRadioButton, runThrough)
#: part/dialogs/KWInsertImage.ui:133 part/dialogs/KWRunAroundProperties.ui:26
#, kde-format
msgid "Run through this shape"
msgstr "浮于形状上"

#. i18n: ectx: property (text), widget (QRadioButton, noRunaround)
#: part/dialogs/KWInsertImage.ui:140 part/dialogs/KWRunAroundProperties.ui:33
#, kde-format
msgid "Skip below this shape"
msgstr "衬于形状下"

#. i18n: ectx: property (text), widget (QRadioButton, left)
#: part/dialogs/KWInsertImage.ui:147 part/dialogs/KWRunAroundProperties.ui:40
#, kde-format
msgid "Run on left side of this shape"
msgstr "形状左侧排列"

#. i18n: ectx: property (text), widget (QRadioButton, right)
#: part/dialogs/KWInsertImage.ui:154 part/dialogs/KWRunAroundProperties.ui:47
#, kde-format
msgid "Run on right side of this shape"
msgstr "形状右侧排列"

#. i18n: ectx: property (text), widget (QRadioButton, longest)
#: part/dialogs/KWInsertImage.ui:161 part/dialogs/KWRunAroundProperties.ui:54
#, kde-format
msgid "Run on longest side of this shape"
msgstr "形状较长侧排列"

#. i18n: ectx: property (text), widget (QRadioButton, both)
#: part/dialogs/KWInsertImage.ui:168 part/dialogs/KWRunAroundProperties.ui:61
#, kde-format
msgid "Run on both sides of this shape"
msgstr "形状两侧排列"

#. i18n: ectx: property (text), widget (QRadioButton, enough)
#: part/dialogs/KWInsertImage.ui:180 part/dialogs/KWRunAroundProperties.ui:73
#, kde-format
msgid "Run on sides that are wider than:"
msgstr "排列在形状单侧长于："

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: part/dialogs/KWInsertImage.ui:194
#, kde-format
msgid "Distance Between Shape and Text:"
msgstr "形状和文本的间距："

#. i18n: ectx: attribute (title), widget (QWidget, columnsTab)
#: part/dialogs/KWPageSettingsDialog.cpp:37 part/dialogs/KWStartupWidget.ui:29
#, kde-format
msgid "Columns"
msgstr "列数"

#: part/dialogs/KWPageSettingsDialog.cpp:42
#, kde-format
msgid "Style"
msgstr "样式"

#: part/dialogs/KWPageSettingsDialog.cpp:48
#, kde-format
msgid "Clone"
msgstr "复制"

#: part/dialogs/KWPageSettingsDialog.cpp:51 part/KWView.cpp:285
#, kde-format
msgid "Delete"
msgstr "删除"

#: part/dialogs/KWPageSettingsDialog.cpp:137
msgctxt "(qtundo-format)"
msgid "Change Page Style"
msgstr "更改页面样式"

#: part/dialogs/KWPageSettingsDialog.cpp:211
#, kde-format
msgid "Add a new page style with the name:"
msgstr "添加新的页面样式名称："

#: part/dialogs/KWPageSettingsDialog.cpp:211
#, kde-format
msgid "Clone Page Style"
msgstr "复制页面样式"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#: part/dialogs/KWRunAroundProperties.ui:17
#, kde-format
msgid "Text elsewhere will:"
msgstr "文本环绕方式："

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: part/dialogs/KWRunAroundProperties.ui:105
#, kde-format
msgid "The shape affects the text by its:"
msgstr "形状影响文本的方式："

#. i18n: ectx: property (text), widget (QRadioButton, box)
#: part/dialogs/KWRunAroundProperties.ui:114
#, kde-format
msgid "Bounding box"
msgstr "边框"

#. i18n: ectx: property (text), widget (QRadioButton, outside)
#: part/dialogs/KWRunAroundProperties.ui:121
#, kde-format
msgid "Contour"
msgstr "轮廓"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: part/dialogs/KWRunAroundProperties.ui:131
#, kde-format
msgid "Shape and text is separated:"
msgstr "形状与文本的分隔方式："

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: part/dialogs/KWRunAroundProperties.ui:140
#, kde-format
msgid "Left by:"
msgstr "左边距："

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: part/dialogs/KWRunAroundProperties.ui:169
#, kde-format
msgid "Above by:"
msgstr "上边距："

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: part/dialogs/KWRunAroundProperties.ui:198
#, kde-format
msgid "Below by:"
msgstr "下边距："

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: part/dialogs/KWRunAroundProperties.ui:227
#, kde-format
msgid "Right by:"
msgstr "右边距："

#. i18n: ectx: property (text), widget (QPushButton, buttonRename)
#: part/dialogs/KWSelectBookmark.ui:20
#, kde-format
msgid "&Rename"
msgstr "重命名(&R)"

#. i18n: ectx: property (text), widget (QPushButton, buttonDelete)
#: part/dialogs/KWSelectBookmark.ui:27
#, kde-format
msgid "&Delete"
msgstr "删除(&D)"

#: part/dialogs/KWSelectBookmarkDialog.cpp:68
#, kde-format
msgid "Rename Bookmark"
msgstr "重命名书签"

#: part/dialogs/KWSelectBookmarkDialog.cpp:69
#, kde-format
msgid "Please provide a new name for the bookmark"
msgstr "请为书签提供一个新名称"

#: part/dialogs/KWSelectBookmarkDialog.cpp:77
#, kde-format
msgid "A bookmark with the name \"%1\" already exists."
msgstr "已存在名为“%1”的书签。"

#: part/dialogs/KWSelectBookmarkDialog.cpp:109
#, kde-format
msgid "Select Bookmark"
msgstr "选择书签"

#: part/dialogs/KWShapeConfigFactory.cpp:44
#, kde-format
msgid "Geometry"
msgstr "几何"

#. i18n: ectx: attribute (title), widget (QWidget, sizeTab)
#: part/dialogs/KWStartupWidget.ui:24
#, kde-format
msgid "Page Size && Margins"
msgstr "页面大小和边距"

#. i18n: ectx: property (text), widget (QPushButton, createButton)
#: part/dialogs/KWStartupWidget.ui:71 part/KWView.cpp:383
#, kde-format
msgid "Create"
msgstr "创建"

#: part/dockers/KWDebugDocker.cpp:18
#, kde-format
msgid "Debug"
msgstr "调试"

#: part/dockers/KWNavigationDocker.cpp:20
#, kde-format
msgid "Navigation"
msgstr "导航"

#: part/dockers/KWNavigationWidget.cpp:95
#, kde-format
msgid "Page #"
msgstr "页 #"

#: part/dockers/KWNavigationWidget.cpp:95
#, kde-format
msgid "Section"
msgstr "节"

#: part/dockers/KWRdfDocker.cpp:39
#, kde-format
msgid "RDF"
msgstr "RDF"

#: part/dockers/KWStatisticsDocker.cpp:20
#, kde-format
msgid "Statistics"
msgstr "统计"

#: part/dockers/KWStatisticsWidget.cpp:128
#, kde-format
msgid "Words:"
msgstr "单词数："

#: part/dockers/KWStatisticsWidget.cpp:131
#, kde-format
msgid "Sentences:"
msgstr "句子数："

#: part/dockers/KWStatisticsWidget.cpp:134
#, kde-format
msgid "Syllables:"
msgstr "音节数："

#: part/dockers/KWStatisticsWidget.cpp:137
#, kde-format
msgid "Characters (spaces):"
msgstr "字符数(包含空格)："

#: part/dockers/KWStatisticsWidget.cpp:140
#, kde-format
msgid "Characters (no spaces):"
msgstr "字符数(不包含空格)："

#: part/dockers/KWStatisticsWidget.cpp:143
#, kde-format
msgid "Lines:"
msgstr "行数："

#: part/dockers/KWStatisticsWidget.cpp:146
#, kde-format
msgid "Readability:"
msgstr "可读性："

#: part/dockers/KWStatisticsWidget.cpp:148
#, kde-format
msgid "Flesch reading ease"
msgstr "Flesch 阅读难易度"

#: part/dockers/KWStatisticsWidget.cpp:150
#, kde-format
msgid "East asian characters:"
msgstr "东亚字符："

#. i18n: ectx: property (text), widget (QCheckBox, check_words)
#: part/dockers/StatisticsPreferencesPopup.ui:17
#, kde-format
msgid "Words"
msgstr "词数"

#. i18n: ectx: property (text), widget (QCheckBox, check_sentences)
#: part/dockers/StatisticsPreferencesPopup.ui:24
#, kde-format
msgid "Sentences"
msgstr "句子数"

#. i18n: ectx: property (text), widget (QCheckBox, check_syllables)
#: part/dockers/StatisticsPreferencesPopup.ui:31
#, kde-format
msgid "Syllables"
msgstr "音节数"

#. i18n: ectx: property (text), widget (QCheckBox, check_lines)
#: part/dockers/StatisticsPreferencesPopup.ui:38
#, kde-format
msgid "Lines"
msgstr "行数"

#. i18n: ectx: property (text), widget (QCheckBox, check_charspace)
#: part/dockers/StatisticsPreferencesPopup.ui:45
#, kde-format
msgid "Characters With Spaces"
msgstr "含空格的字符数"

#. i18n: ectx: property (text), widget (QCheckBox, check_charnospace)
#: part/dockers/StatisticsPreferencesPopup.ui:52
#, kde-format
msgid "Characters without spaces"
msgstr "不含空格的字符数"

#. i18n: ectx: property (text), widget (QCheckBox, check_east)
#: part/dockers/StatisticsPreferencesPopup.ui:59
#, kde-format
msgid "East Asian characters"
msgstr "东亚字符"

#. i18n: ectx: property (text), widget (QCheckBox, check_flesch)
#: part/dockers/StatisticsPreferencesPopup.ui:66
#, kde-format
msgid "Flesch Reading Ease"
msgstr "Flesch 阅读难易度"

#: part/i18n/stylenames.cpp:8
#, kde-format
msgctxt "Style name"
msgid "Standard"
msgstr "标准"

#: part/i18n/stylenames.cpp:9
#, kde-format
msgctxt "Style name"
msgid "Head 1"
msgstr "一级标题"

#: part/i18n/stylenames.cpp:10
#, kde-format
msgctxt "Style name"
msgid "Head 2"
msgstr "二级标题"

#: part/i18n/stylenames.cpp:11
#, kde-format
msgctxt "Style name"
msgid "Head 3"
msgstr "三级标题"

#: part/i18n/stylenames.cpp:12
#, kde-format
msgctxt "Style name"
msgid "Enumerated List"
msgstr "枚举列表"

#: part/i18n/stylenames.cpp:13
#, kde-format
msgctxt "Style name"
msgid "Alphabetical List"
msgstr "字序列表"

#: part/i18n/stylenames.cpp:14
#, kde-format
msgctxt "Style name"
msgid "Bullet List"
msgstr "项目符号"

#: part/i18n/stylenames.cpp:15
#, kde-format
msgctxt "Style name"
msgid "Contents Title"
msgstr "目录标题"

#: part/i18n/stylenames.cpp:16
#, kde-format
msgctxt "Style name"
msgid "Contents Head 1"
msgstr "目录一级标题"

#: part/i18n/stylenames.cpp:17
#, kde-format
msgctxt "Style name"
msgid "Contents Head 2"
msgstr "目录二级标题"

#: part/i18n/stylenames.cpp:18
#, kde-format
msgctxt "Style name"
msgid "Contents Head 3"
msgstr "目录三级标题"

#: part/i18n/stylenames.cpp:19
#, kde-format
msgctxt "Style name"
msgid "Document Title"
msgstr "文档标题"

#: part/i18n/stylenames.cpp:20
#, kde-format
msgctxt "Style name"
msgid "Header"
msgstr "页眉"

#: part/i18n/stylenames.cpp:21
#, kde-format
msgctxt "Style name"
msgid "Footer"
msgstr "页脚"

#: part/i18n/stylenames.cpp:24
#, kde-format
msgctxt "Style name"
msgid "Plain"
msgstr "纯文本"

#: part/i18n/stylenames.cpp:25
#, kde-format
msgctxt "Style name"
msgid "Borders 1"
msgstr "边框一"

#: part/i18n/stylenames.cpp:26
#, kde-format
msgctxt "Style name"
msgid "Borders 2"
msgstr "边框二"

#: part/i18n/stylenames.cpp:27
#, kde-format
msgctxt "Style name"
msgid "Borders 3"
msgstr "边框三"

#: part/i18n/stylenames.cpp:28
#, kde-format
msgctxt "Style name"
msgid "Row"
msgstr "行"

#: part/i18n/stylenames.cpp:29
#, kde-format
msgctxt "Style name"
msgid "Column"
msgstr "列"

#: part/i18n/stylenames.cpp:30
#, kde-format
msgctxt "Style name"
msgid "Light Gray"
msgstr "浅灰"

#: part/i18n/stylenames.cpp:31
#, kde-format
msgctxt "Style name"
msgid "Dark Gray"
msgstr "深灰"

#: part/i18n/stylenames.cpp:32
#, kde-format
msgctxt "Style name"
msgid "Black"
msgstr "黑"

#: part/i18n/stylenames.cpp:33
#, kde-format
msgctxt "Style name"
msgid "Light Blue"
msgstr "浅蓝"

#: part/i18n/stylenames.cpp:34
#, kde-format
msgctxt "Style name"
msgid "Dark Blue"
msgstr "深蓝"

#: part/i18n/stylenames.cpp:35
#, kde-format
msgctxt "Style name"
msgid "Red"
msgstr "红"

#: part/i18n/stylenames.cpp:36
#, kde-format
msgctxt "Style name"
msgid "Yellow"
msgstr "黄"

#: part/i18n/stylenames.cpp:37
#, kde-format
msgctxt "Style name"
msgid "Colorful"
msgstr "多彩"

#: part/i18n/stylenames.cpp:38
#, kde-format
msgctxt "Style name"
msgid "Bluish"
msgstr "蓝"

#: part/i18n/stylenames.cpp:41
#, kde-format
msgctxt "Style name"
msgid "Simple 1"
msgstr "简单一"

#: part/i18n/stylenames.cpp:42
#, kde-format
msgctxt "Style name"
msgid "Simple 2"
msgstr "简单二"

#: part/i18n/stylenames.cpp:43
#, kde-format
msgctxt "Style name"
msgid "Simple 3"
msgstr "简单三"

#: part/i18n/stylenames.cpp:44
#, kde-format
msgctxt "Style name"
msgid "Header 1"
msgstr "标题一"

#: part/i18n/stylenames.cpp:45
#, kde-format
msgctxt "Style name"
msgid "Header 2"
msgstr "标题二"

#: part/i18n/stylenames.cpp:46
#, kde-format
msgctxt "Style name"
msgid "Header 3"
msgstr "标题三"

#: part/i18n/stylenames.cpp:47
#, kde-format
msgctxt "Style name"
msgid "Header 4"
msgstr "标题四"

#: part/i18n/stylenames.cpp:50
#, kde-format
msgctxt "Style name"
msgid "Columns 1"
msgstr "列一"

#: part/i18n/stylenames.cpp:51
#, kde-format
msgctxt "Style name"
msgid "Columns 2"
msgstr "列二"

#: part/i18n/stylenames.cpp:52
#, kde-format
msgctxt "Style name"
msgid "Grid 1"
msgstr "网格一"

#: part/i18n/stylenames.cpp:53
#, kde-format
msgctxt "Style name"
msgid "Grid 2"
msgstr "网格二"

#: part/i18n/stylenames.cpp:54
#, kde-format
msgctxt "Style name"
msgid "Grid 3"
msgstr "网格三"

#: part/i18n/stylenames.cpp:55
#, kde-format
msgctxt "Style name"
msgid "Gray Heading"
msgstr "灰色标题"

#: part/i18n/stylenames.cpp:56
#, kde-format
msgctxt "Style name"
msgid "Blue Heading"
msgstr "蓝色标题"

#: part/i18n/stylenames.cpp:57
#, kde-format
msgctxt "Style name"
msgid "Blue Traditional"
msgstr "传统蓝色"

#: part/i18n/stylenames.cpp:58
#, kde-format
msgctxt "Style name"
msgid "Gray Traditional"
msgstr "传统灰色"

#: part/i18n/stylenames.cpp:59
#, kde-format
msgctxt "Style name"
msgid "Blue Top and Bottom"
msgstr "蓝色顶部和底部"

#: part/i18n/stylenames.cpp:60
#, kde-format
msgctxt "Style name"
msgid "Gray Top and Bottom"
msgstr "灰色顶部和底部"

#: part/KWAboutData.h:20
#, kde-format
msgctxt "application name"
msgid "Calligra Words"
msgstr "Calligra Words"

#: part/KWAboutData.h:22
#, kde-format
msgid "Word processor"
msgstr "字处理程序"

#: part/KWAboutData.h:24
#, kde-format
msgid "Copyright 1998-%1, The Words Team"
msgstr "版权所有 1998-%1，Words团队"

#: part/KWAboutData.h:32 part/KWAboutData.h:33
#, kde-format
msgid "Co maintainer"
msgstr "协助维护者"

#: part/KWAboutData.h:32
#, kde-format
msgid "Pierre Ducroquet"
msgstr "Pierre Ducroquet"

#: part/KWAboutData.h:33
#, kde-format
msgid "C. Boemann"
msgstr "C. Boemann"

#: part/KWAboutData.h:34 part/KWAboutData.h:35 part/KWAboutData.h:36
#: part/KWAboutData.h:38
#, kde-format
msgid "Everything"
msgstr "所有事情"

#: part/KWAboutData.h:34
#, kde-format
msgid "Sebastian Sauer"
msgstr "Sebastian Sauer"

#: part/KWAboutData.h:35
#, kde-format
msgid "Boudewijn Rempt"
msgstr "Boudewijn Rempt"

#: part/KWAboutData.h:36
#, kde-format
msgid "Pierre Stirnweiss"
msgstr "Pierre Stirnweiss"

#: part/KWAboutData.h:37
#, kde-format
msgid "Formatting stuff"
msgstr "格式化工作"

#: part/KWAboutData.h:37
#, kde-format
msgid "Inge Wallin"
msgstr "Inge Wallin"

#: part/KWAboutData.h:38
#, kde-format
msgid "Thorsten Zachmann"
msgstr "Thorsten Zachmann"

#: part/KWAboutData.h:39 part/KWAboutData.h:45 part/KWAboutData.h:46
#: part/KWAboutData.h:47 part/KWAboutData.h:59 part/KWAboutData.h:60
#: part/KWAboutData.h:61 part/KWAboutData.h:62 part/KWAboutData.h:63
#: part/KWAboutData.h:64 part/KWAboutData.h:65 part/KWAboutData.h:66
#: part/KWAboutData.h:67 part/KWAboutData.h:68
#, kde-format
msgid "Filter"
msgstr "过滤器"

#: part/KWAboutData.h:39
#, kde-format
msgid "Matus Uzak"
msgstr "Matus Uzak"

#: part/KWAboutData.h:40
#, kde-format
msgid "Layout and Painting"
msgstr "布局和绘图"

#: part/KWAboutData.h:40
#, kde-format
msgid "Pavol Korinek"
msgstr "Pavol Korinek"

#: part/KWAboutData.h:41
#, kde-format
msgid "Shreya Pandit"
msgstr "Shreya Pandit"

#: part/KWAboutData.h:41
#, kde-format
msgid "Statistics docker"
msgstr "统计窗格"

#: part/KWAboutData.h:42
#, kde-format
msgid "Brijesh Patel"
msgstr "Brijesh Patel"

#: part/KWAboutData.h:42
#, kde-format
msgid "Foot and endnotes"
msgstr "脚注和尾注"

#: part/KWAboutData.h:43
#, kde-format
msgid "Bibliography"
msgstr "参考文献"

#: part/KWAboutData.h:43
#, kde-format
msgid "Smit Patel"
msgstr "Smit Patel"

#: part/KWAboutData.h:44
#, kde-format
msgid "Mojtaba Shahi"
msgstr "Mojtaba Shahi"

#: part/KWAboutData.h:44
#, kde-format
msgid "Style Manager"
msgstr "样式管理器"

#: part/KWAboutData.h:45
#, kde-format
msgid "Lassi Nieminen"
msgstr "Lassi Nieminen"

#: part/KWAboutData.h:46
#, kde-format
msgid "Hanzes Matus"
msgstr "Hanzes Matus"

#: part/KWAboutData.h:47
#, kde-format
msgid "Lukáš Tvrdý"
msgstr "Lukáš Tvrdý"

#: part/KWAboutData.h:48
#, kde-format
msgid "Thomas Zander"
msgstr "Thomas Zander"

#: part/KWAboutData.h:49
#, kde-format
msgid "Girish Ramakrishnan"
msgstr "Girish Ramakrishnan"

#: part/KWAboutData.h:49 part/KWAboutData.h:50
#, kde-format
msgid "ODF Support"
msgstr "ODF 支持"

#: part/KWAboutData.h:50
#, kde-format
msgid "Robert Mathias Marmorstein"
msgstr "Robert Mathias Marmorstein"

#: part/KWAboutData.h:51
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: part/KWAboutData.h:52
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: part/KWAboutData.h:53
#, kde-format
msgid "Sven Lüppken"
msgstr "Sven Lüppken"

#: part/KWAboutData.h:54
#, kde-format
msgid "Frank Dekervel"
msgstr "Frank Dekervel"

#: part/KWAboutData.h:55
#, kde-format
msgid "Krister Wicksell Eriksson"
msgstr "Krister Wicksell Eriksson"

#: part/KWAboutData.h:56
#, kde-format
msgid "Dag Andersen"
msgstr "Dag Andersen"

#: part/KWAboutData.h:57
#, kde-format
msgid "Nash Hoogwater"
msgstr "Nash Hoogwater"

#: part/KWAboutData.h:58
#, kde-format
msgid "KFormula"
msgstr "KFormula"

#: part/KWAboutData.h:58
#, kde-format
msgid "Ulrich Kuettler"
msgstr "Ulrich Kuettler"

#: part/KWAboutData.h:59
#, kde-format
msgid "Shaheed Haque"
msgstr "Shaheed Haque"

#: part/KWAboutData.h:60
#, kde-format
msgid "Werner Trobin"
msgstr "Werner Trobin"

#: part/KWAboutData.h:61
#, kde-format
msgid "Nicolas Goutte"
msgstr "Nicolas Goutte"

#: part/KWAboutData.h:62
#, kde-format
msgid "Ariya Hidayat"
msgstr "Ariya Hidayat"

#: part/KWAboutData.h:63
#, kde-format
msgid "Clarence Dang"
msgstr "Clarence Dang"

#: part/KWAboutData.h:64
#, kde-format
msgid "Robert Jacolin"
msgstr "Robert Jacolin"

#: part/KWAboutData.h:65
#, kde-format
msgid "Enno Bartels"
msgstr "Enno Bartels"

#: part/KWAboutData.h:66
#, kde-format
msgid "Ewald Snel"
msgstr "Ewald Snel"

#: part/KWAboutData.h:67
#, kde-format
msgid "Tomasz Grobelny"
msgstr "Tomasz Grobelny"

#: part/KWAboutData.h:68
#, kde-format
msgid "Michael Johnson"
msgstr "Michael Johnson"

#: part/KWAboutData.h:69
#, kde-format
msgid "Fatcow Web Hosting"
msgstr "Fatcow Web 主机"

#: part/KWAboutData.h:69
#, kde-format
msgid "Page break icon"
msgstr "分页图标"

#. i18n translator strings
#: part/KWAboutData.h:71
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE 中国"

#: part/KWAboutData.h:72
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org"

#: part/KWDocument.cpp:495
#, kde-format
msgid "Copy"
msgstr "复制"

#: part/KWDocument.cpp:540 part/KWStatusBar.cpp:146
#, kde-format
msgid "Standard"
msgstr "标准"

#: part/KWDocument.cpp:546
#, kde-format
msgid "Document Title"
msgstr "文档标题"

#: part/KWDocument.cpp:553
#, kde-format
msgid "Head 1"
msgstr "一级标题"

#: part/KWDocument.cpp:559
#, kde-format
msgid "Head 2"
msgstr "二级标题"

#: part/KWDocument.cpp:565
#, kde-format
msgid "Head 3"
msgstr "三级标题"

#: part/KWDocument.cpp:571
#, kde-format
msgid "Bullet List"
msgstr "项目符号"

#: part/KWGui.cpp:69
#, kde-format
msgid "Tools"
msgstr "工具"

#: part/KWOdfLoader.cpp:83
#, kde-format
msgid "Invalid OASIS OpenDocument file. No office:body tag found."
msgstr "无效的 OASIS OpenDocument 文件。未找到 office:body 标签。"

#: part/KWOdfLoader.cpp:95
#, kde-format
msgid "Invalid OASIS OpenDocument file. No tag found inside office:body."
msgstr "无效的 OASIS OpenDocument 文件。office:body 标签内未找到标签。"

#: part/KWOdfLoader.cpp:97
#, kde-format
msgid ""
"This is not a word processing document, but %1. Please try opening it with "
"the appropriate application."
msgstr "这不是一个字处理文档，而是%1。请试着使用相应的应用程序打开它。"

#: part/KWPageManager.cpp:24 part/KWPageManager.cpp:413
#, kde-format
msgctxt "Default page style display name"
msgid "Standard"
msgstr ""

#: part/KWPart.cpp:141
#, kde-format
msgid "Can not find needed text component, Words will quit now"
msgstr "找不到所需的文本组件，Words 将会退出"

#: part/KWPart.cpp:142
#, kde-format
msgid "Installation Error"
msgstr "安装错误"

#: part/KWStatusBar.cpp:35
#, kde-format
msgid "Modified"
msgstr "已修改"

#: part/KWStatusBar.cpp:36
#, kde-format
msgid "Saved"
msgstr "已保存"

#: part/KWStatusBar.cpp:37
#, kde-format
msgid "Page %1 of %2"
msgstr "第%1页/共%2页"

#: part/KWStatusBar.cpp:38
#, kde-format
msgid "Page %1-%2 of %3"
msgstr "第%1-%2页/共%3页"

#: part/KWStatusBar.cpp:39
#, kde-format
msgid "Line %1"
msgstr "第%1行"

#: part/KWStatusBar.cpp:124
#, kde-format
msgid "Page Number"
msgstr "页数"

#: part/KWStatusBar.cpp:137
#, kde-format
msgid "Line Number"
msgstr "行数"

#: part/KWStatusBar.cpp:149
#, kde-format
msgid "Change the page style"
msgstr "更改页面样式"

#: part/KWStatusBar.cpp:155
#, kde-format
msgid "Page Style"
msgstr "页面样式"

#: part/KWStatusBar.cpp:169
#, kde-format
msgid "Page Size"
msgstr "页面大小"

#: part/KWStatusBar.cpp:184
#, kde-format
msgid "Saved/Modified"
msgstr "已保存/已修改"

#: part/KWStatusBar.cpp:197
#, kde-format
msgid "Mouse Cursor X:Y"
msgstr "鼠标位置 X:Y"

#: part/KWStatusBar.cpp:211
#, kde-format
msgid "Zoom Controller"
msgstr "缩放控制器"

#: part/KWView.cpp:183
#, kde-format
msgid "Exit Fullscreen Mode"
msgstr "退出全屏模式"

#: part/KWView.cpp:260
#, kde-format
msgid "Shape Properties..."
msgstr "形状属性..."

#: part/KWView.cpp:262
#, kde-format
msgid "Change how the shape behave"
msgstr "更改形状行为"

#: part/KWView.cpp:271
#, kde-format
msgid "Create Template From Document..."
msgstr "从文档创建模板..."

#: part/KWView.cpp:272
#, kde-format
msgid "Save this document and use it later as a template"
msgstr "保存此文档并用作模板"

#: part/KWView.cpp:273
#, kde-format
msgid ""
"You can save this document as a template.<br><br>You can use this new "
"template as a starting point for another document."
msgstr ""
"您可以保存此文档为模板。<br><br>您可以使用此新模板作为另一个文档的起点。"

#: part/KWView.cpp:292
#, kde-format
msgid "Show Formatting Characters"
msgstr "显示格式化字符"

#: part/KWView.cpp:298
#, kde-format
msgid "Toggle the display of non-printing characters"
msgstr "切换非打印字符的显示"

#: part/KWView.cpp:299
#, kde-format
msgid ""
"Toggle the display of non-printing characters.<br/><br/>When this is "
"enabled, Words shows you tabs, spaces, carriage returns and other non-"
"printing characters."
msgstr ""
"切换非打印字符的显示/隐藏。<br/><br/>当它被启用，Words 向您显示制表符、空格、"
"换行符、以及其他非打印字符。"

#: part/KWView.cpp:301
#, kde-format
msgid "Show Field Shadings"
msgstr "显示字段阴影"

#: part/KWView.cpp:307
#, kde-format
msgid "Toggle the shaded background of fields"
msgstr "切换字段的背景阴影"

#: part/KWView.cpp:308
#, kde-format
msgid ""
"Toggle the visualization of fields (variables etc.) by drawing their "
"background in a contrasting color."
msgstr "通过高对比度的颜色绘制字段 (变量等) 的背景来切换可视化效果。"

#: part/KWView.cpp:310
#, kde-format
msgid "Show Text Shape Borders"
msgstr "显示文本形状边框"

#: part/KWView.cpp:311
#, kde-format
msgid "Turns the border display on and off"
msgstr "打开或关闭框显示"

#: part/KWView.cpp:317
#, kde-format
msgid ""
"Turns the border display on and off.<br/><br/>The borders are never printed. "
"This option is useful to see how the document will appear on the printed "
"page."
msgstr ""
"关闭和打开边框显示。<br/><br/>边框从不会被打印。这个选项对观察文档如何在打印"
"页面上显示很有用。"

#: part/KWView.cpp:319
#, kde-format
msgid "Show Table Borders"
msgstr "显示表格边框"

#: part/KWView.cpp:325
#, kde-format
msgid "Toggle the display of table borders"
msgstr "切换表格边框的显示"

#: part/KWView.cpp:326
#, kde-format
msgid ""
"Toggle the display of table borders.<br/><br/>When this is enabled, Words "
"shows you any invisible table borders with a thin gray line."
msgstr ""
"切换表格边框的显示。<br/><br/>当它被启用，Words 将以灰色细线显示不可见的表格"
"边框。"

#: part/KWView.cpp:328
#, kde-format
msgid "Show Section Bounds"
msgstr "显示截面边界"

#: part/KWView.cpp:334
#, kde-format
msgid "Toggle the display of section bounds"
msgstr "切换截面边界显示"

#: part/KWView.cpp:335
#, kde-format
msgid ""
"Toggle the display of section bounds.<br/><br/>When this is enabled, any "
"section bounds will be indicated with a thin gray horizontal brackets."
msgstr ""
"切换表格边框的显示。<br/><br/>当它被启用，Words 将以灰色细线显示不可见的表格"
"边框。"

#: part/KWView.cpp:337
#, kde-format
msgid "Show Rulers"
msgstr "显示标尺"

#: part/KWView.cpp:339
#, kde-format
msgid "Shows or hides rulers"
msgstr "显示或隐藏标尺"

#: part/KWView.cpp:340
#, kde-format
msgid ""
"The rulers are the white measuring spaces top and left of the document. The "
"rulers show the position and width of pages and of frames and can be used to "
"position tabulators among others.<p>Uncheck this to disable the rulers from "
"being displayed.</p>"
msgstr ""
"标尺是在文档顶部和左侧的白色计量空格。标尺显示页面和框架的位置和宽度，可以被"
"用来定位制表符。<p>取消该项会禁用标尺的显示。</p>"

#: part/KWView.cpp:351
#, kde-format
msgid "Snap to Grid"
msgstr "吸附到网格"

#: part/KWView.cpp:359
#, kde-format
msgid "Show Status Bar"
msgstr "显示状态栏"

#: part/KWView.cpp:360
#, kde-format
msgid "Shows or hides the status bar"
msgstr "显示或隐藏状态栏"

#: part/KWView.cpp:366
#, kde-format
msgid "Fullscreen Mode"
msgstr "全屏模式"

#: part/KWView.cpp:367
#, kde-format
msgid "Set view in fullscreen mode"
msgstr ""

#: part/KWView.cpp:374
#, kde-format
msgid "Semantic Stylesheets..."
msgstr "语义学样式表..."

#: part/KWView.cpp:376
#, kde-format
msgid "Modify and add semantic stylesheets"
msgstr "修改并添加语义学样式表"

#: part/KWView.cpp:377
#, kde-format
msgid ""
"Stylesheets are used to format the display of information which is stored in "
"RDF."
msgstr "用来格式化联系人、事件和位置信息的样式表，以 Rdf 格式存储"

#: part/KWView.cpp:392
#, kde-format
msgid "Configure..."
msgstr "配置..."

#: part/KWView.cpp:401
#, kde-format
msgid "Page Layout..."
msgstr "页面布局..."

#: part/KWView.cpp:403
#, kde-format
msgid "Change properties of entire page"
msgstr "更改整个页面的属性"

#: part/KWView.cpp:404
#, kde-format
msgid ""
"Change properties of the entire page.<p>Currently you can change paper size, "
"paper orientation, header and footer sizes, and column settings.</p>"
msgstr ""
"更改整个页面的属性。<p>当前您可以更改纸张大小、纸张方向、页眉和页脚大小，以及"
"分栏设置。</p>"

#: part/KWView.cpp:407
#, kde-format
msgid "Create Header"
msgstr "创建页眉"

#: part/KWView.cpp:413
#, kde-format
msgid "Create Footer"
msgstr "创建页脚"

#: part/KWView.cpp:420
#, kde-format
msgid "Show Comments"
msgstr "显示注释"

#: part/KWView.cpp:421
#, kde-format
msgid "Shows comments in the document"
msgstr "显示文档中的注释"

#: part/KWView.cpp:427
#, kde-format
msgid "Word Count"
msgstr "单词计数"

#: part/KWView.cpp:428
#, kde-format
msgid "Shows or hides word counting in status bar"
msgstr "在状态栏中显示或隐藏单词计数"

#: part/pagetool/KWPageTool.cpp:85
#, kde-format
msgid "Page Setup"
msgstr "页面设置"

#: part/pagetool/KWPageTool.cpp:89
#, kde-format
msgid "Header & Footer"
msgstr "页眉和页脚"

#: part/pagetool/KWPageToolFactory.cpp:17
#, kde-format
msgid "Page layout"
msgstr "页面布局"

#. i18n: ectx: property (text), widget (QToolButton, setup)
#. i18n: ectx: property (text), widget (QToolButton, insertHeader)
#. i18n: ectx: property (text), widget (QToolButton, insertFooter)
#: part/pagetool/SimpleHeaderFooterWidget.ui:17
#: part/pagetool/SimpleHeaderFooterWidget.ui:24
#: part/pagetool/SimpleSetupWidget.ui:17
#, kde-format
msgid "..."
msgstr "..."

#: part/widgets/KoFindToolbar.cpp:55
#, kde-format
msgctxt "Label for the Find text input box"
msgid "Find:"
msgstr "查找："

#: part/widgets/KoFindToolbar.cpp:68
#, kde-format
msgctxt "Next search result"
msgid "Next"
msgstr "下次执行"

#: part/widgets/KoFindToolbar.cpp:78
#, kde-format
msgctxt "Previous search result"
msgid "Previous"
msgstr "上一首"

#: part/widgets/KoFindToolbar.cpp:87
#, kde-format
msgctxt "Search options"
msgid "Options"
msgstr "选项"

#: part/widgets/KoFindToolbar.cpp:115
#, kde-format
msgctxt "Label for the Replace text input box"
msgid "Replace:"
msgstr "替换："

#: part/widgets/KoFindToolbar.cpp:125
#, kde-format
msgctxt "Replace the current match"
msgid "Replace"
msgstr "替换"

#: part/widgets/KoFindToolbar.cpp:131
#, kde-format
msgctxt "Replace all found matches"
msgid "Replace All"
msgstr "全部替换"

#: part/widgets/KoFindToolbar.cpp:139
#, kde-format
msgid "Replace"
msgstr "替换"

#: part/widgets/KoFindToolbar.cpp:200
#, kde-format
msgctxt "Total number of matches"
msgid "1 match found"
msgid_plural "%1 matches found"
msgstr[0] "找到了 %1 处匹配"

#: part/widgets/KoFindToolbar.cpp:210
#, kde-format
msgid "No matches found"
msgstr "没有匹配的结果"

#: part/widgets/KoFindToolbar.cpp:216
#, kde-format
msgid "Search hit bottom, continuing from top."
msgstr "搜索到达底部，从顶部继续。"

#: part/widgets/KoFindToolbar.cpp:218
#, kde-format
msgid "Search hit top, continuing from bottom."
msgstr "搜索到达顶部，从底部继续。"

#: part/Words.cpp:45
#, kde-format
msgid "Odd Pages Header"
msgstr "奇数页页眉"

#: part/Words.cpp:47
#, kde-format
msgid "Even Pages Header"
msgstr "偶数页页眉"

#: part/Words.cpp:49
#, kde-format
msgid "Odd Pages Footer"
msgstr "奇数页页脚"

#: part/Words.cpp:51
#, kde-format
msgid "Even Pages Footer"
msgstr "偶数页页脚"

#: part/Words.cpp:53
#, kde-format
msgid "Main text"
msgstr "主要文本"

#: part/Words.cpp:55
#, kde-format
msgid "Other text"
msgstr "其他文本"
